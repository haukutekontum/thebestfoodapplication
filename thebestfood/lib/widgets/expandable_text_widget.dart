import 'package:flutter/material.dart';
import 'package:thebestfood/utils/colors.dart';
import 'package:thebestfood/utils/dimension.dart';
import 'package:thebestfood/widgets/small_text.dart';

class ExpandableTextWidget extends StatefulWidget {
  final String text;
  const ExpandableTextWidget(
      {super.key, required this.text});

  @override
  State<ExpandableTextWidget> createState() => _State();
}

class _State extends State<ExpandableTextWidget> {
  late String firstHalf;
  late String secondHalf;

  bool heddenText = true;

  double textHeight = Dimension.screenHeight / 5.63;

  @override
  void initState() {
    super.initState();
    if (widget.text.length > textHeight) {
      firstHalf =
          widget.text.substring(0, textHeight.toInt());
      secondHalf = widget.text.substring(
          textHeight.toInt() + 1, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: secondHalf.isEmpty
          ? SmallText(text: firstHalf)
          : Column(
              children: [
                SmallText(
                    size: Dimension.font16,
                    height: 1.8,
                    color: AppColors.paraColor,
                    text: heddenText
                        ? (firstHalf + "...")
                        : (firstHalf + secondHalf)),
                InkWell(
                  onTap: () {
                    setState(() {
                      heddenText = !heddenText;
                    });
                  },
                  child: Row(
                    children: [
                      SmallText(
                        size: Dimension.font16,
                        text: "Show more",
                        color: AppColors.mainColor,
                      ),
                      Icon(
                        heddenText
                            ? Icons.arrow_drop_down
                            : Icons.arrow_drop_up,
                        color: AppColors.mainColor,
                      )
                    ],
                  ),
                )
              ],
            ),
    );
  }
}
