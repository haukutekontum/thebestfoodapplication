import 'package:flutter/material.dart';
import 'package:thebestfood/utils/colors.dart';
import 'package:thebestfood/utils/dimension.dart';
import 'package:thebestfood/widgets/big_text.dart';
import 'package:thebestfood/widgets/icon_and_text_widget.dart';
import 'package:thebestfood/widgets/small_text.dart';

class AppColumn extends StatelessWidget {
  final String text;

  const AppColumn({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BigText(
            text: text,
            size: Dimension.font26,
          ),
          SizedBox(
            height: Dimension.font10,
          ),
          Row(
            children: [
              Wrap(
                  children: List.generate(5, (index) {
                return Icon(Icons.star,
                    color: AppColors.mainColor,
                    size: Dimension.iconSize15);
              })),
              SizedBox(
                width: Dimension.font10,
              ),
              SmallText(text: "4.5"),
              SizedBox(
                width: Dimension.font10,
              ),
              SmallText(text: "1287"),
              SizedBox(
                width: Dimension.font10,
              ),
              SmallText(text: "comments")
            ],
          ),
          SizedBox(
            height: Dimension.height20,
          ),
          Row(
            mainAxisAlignment:
                MainAxisAlignment.spaceBetween,
            children: [
              IconAndTextWidget(
                  icon: Icons.circle_sharp,
                  text: "Nomal",
                  iconColor: AppColors.iconColor1),
              IconAndTextWidget(
                  icon: Icons.location_on,
                  text: "1.7 km",
                  iconColor: AppColors.mainColor),
              IconAndTextWidget(
                  icon: Icons.access_time_rounded,
                  text: "32 min",
                  iconColor: AppColors.iconColor2)
            ],
          )
        ]);
  }
}
