import 'package:get/get.dart';
import 'package:thebestfood/pages/cart/cart_page.dart';
import 'package:thebestfood/pages/food/popular_food_detail.dart';
import 'package:thebestfood/pages/food/recommended_food_detail.dart';
import 'package:thebestfood/pages/home/home_page.dart';
import 'package:thebestfood/pages/home/main_food_page.dart';
import 'package:thebestfood/pages/splash/splash_page.dart';

class RouteHelper {
  static const String splashPage = "/splash-page";
  static const String initial = "/";
  static const String popularFood = "/popular-food";
  static const String recommendedFood = "/recommended-food";
  static const String cartPage = "/cart-page";

  static String getSplashPage()=>'$splashPage';
  static String getInitial() => '$initial';
  static String getPopularFood(int pageId, String page) =>
      '$popularFood?pageId=$pageId&page=$page';
  static String getRecommendedFood(int pageId, String page) =>
      '$recommendedFood?pageId=$pageId&page=$page';
  static String getCartPage() => '$cartPage';

  static List<GetPage> routes = [
    //Splash Page
    GetPage(name: splashPage, page: ()=>SplashScreen()),

    //Page home
    GetPage(name: initial, page: () => HomePage()),

    //Page Popular Food
    GetPage(
        name: popularFood,
        page: () {
          var pageId = Get.parameters['pageId'];
          var page = Get.parameters["page"];
          return PopularFoodDetail(
              pageId:
                  //try convert string pageId to int
                  //can not equal 0 cuz the first item in list is 0
                  int.tryParse(pageId ?? '') ?? -1,
              page: page!);
        },
        transition: Transition.fadeIn),

    GetPage(
      name: recommendedFood,
      page: () {
        var pageId = Get.parameters['pageId'];
        var page = Get.parameters['page'];
        return RecommenedFoodDetail(
            pageId: int.tryParse(pageId ?? '') ?? -1, page: page!);
      },
    ),

    GetPage(
        name: cartPage,
        page: () {
          return CartPage();
        },
        transition: Transition.fadeIn)
  ];
}
