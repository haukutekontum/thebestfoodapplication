import 'package:get/get.dart';

class Dimension {
  static double screenHeight = Get.context!.height;
  static double screenWidth = Get.context!.width;

  static double pageView = screenHeight / 2.64;
  static double pageViewContainer = screenHeight / 3.84;
  static double pageViewTextContainer = screenHeight / 7.03;

//dynamic height and margin
  static double height10 = screenHeight / 84.4;
  static double height20 = screenHeight / 42.2;
  static double height15 = screenHeight / 56.3;
  static double height30 = screenHeight / 28.13;
  static double height45 = screenHeight / 18.76;

//dynamic width and margin
  static double width10 = screenHeight / 84.4;
  static double width15 = screenHeight / 56.3;
  static double width20 = screenHeight / 42.2;
  static double width30 = screenHeight / 28.13;
  static double width45 = screenHeight / 18.76;

// font size
  static double font10 = screenHeight / 84.4;
  static double font16 = screenHeight / 52.75;
  static double font20 = screenHeight / 42.2;
  static double font26 = screenHeight / 32.4;

//dynamic radious
  static double radious15 = screenHeight / 56.3;
  static double radious20 = screenHeight / 42.2;
  static double radious30 = screenHeight / 28.13;

//dynamic icon size
  static double iconSize15 = screenHeight / 56.3;
  static double iconSize24 = screenHeight / 35.17;
  static double iconSize16 = screenHeight / 52.75;

  //listview view size
  static double listViewImgSize = screenWidth / 3.25;
  static double listViewTextContSize = screenHeight / 3.9;

  //popular food
  static double popularFoodImgSize = screenHeight / 2.41;

  //bottom height
  static double bottomHeightBar = screenHeight / 6.93;
}
