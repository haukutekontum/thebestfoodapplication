class AppConstants {
  static const String APP_NAME = "DBFood";
  static const int APP_VERSION = 1;

  static const String BASE_URL = "http://localhost:3000";
  static const String POPULAR_PRODUCT_URL = "/product";
  static const String RECOMMENDED_PRODUCT_URL = "/recommended";

  static const String TOKEN = "DBtoken";
  static const String CART_LIST = "cart-list";
  static const String CART_HISTORY_LIST = "cart-history-list";
}
