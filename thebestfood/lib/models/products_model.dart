class Product {
  int? _totalSize;
  int? _typeId;
  int? _offset;
  late List<ProductModel> _products;
  List<ProductModel> get products => _products;

  Product(
      {required totalSize,
      required offset,
      required typeId,
      required products}) {
    this._totalSize = totalSize;
    this._offset = offset;
    this._typeId = typeId;
    this._products = products;
  }

  Product.fromJson(Map<String, dynamic> json) {
    _totalSize = json['total_size'];
    _typeId = json['type_id'];
    _offset = json['offset'];
    if (json['products'] != null) {
      _products = <ProductModel>[];
      json['products'].forEach((v) {
        _products!.add(ProductModel.fromJson(v));
      });
    }
  }
}

class ProductModel {
  int? id;
  String? name;
  String? description;
  int? price;
  int? stars;
  String? img;
  String? location;
  String? createAt;
  String? updateAt;
  int? typeId;

  ProductModel(
      {required this.id,
      this.name,
      this.description,
      this.createAt,
      this.img,
      this.location,
      this.price,
      this.stars,
      this.typeId,
      this.updateAt});

  ProductModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    createAt = json['createAt'];
    img = json['img'];
    location = json['location'];
    price = json['price'];
    stars = json['stars'];
    typeId = json['typeId'];
    updateAt = json['updateAt'];
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "price": this.price,
      "description": this.description,
      "createAt": this.createAt,
      "img": this.img,
      "location": this.location,
      "stars": this.stars,
      "typeId": this.typeId,
      "updateAt": this.updateAt,
    };
  }
}
