import 'package:thebestfood/models/products_model.dart';

class CartModel {
  int? id;
  String? name;
  int? price;
  String? img;
  int? quantity;
  bool? isExist;
  String? time;
  ProductModel? product;

  CartModel(
      {this.id,
      this.name,
      this.img,
      this.price,
      this.quantity,
      this.isExist,
      this.time,
      this.product});

  CartModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    img = json['img'];
    price = json['price'];
    quantity = json['quantity'];
    isExist = json['isExist'];
    time = json['time'];
    product = ProductModel.fromJson(json['product']);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "price": this.price,
      "quantity": this.quantity,
      "isExist": this.isExist,
      "time": this.time,
      "product": this.product!.toJson()
    };
  }

  @override
  String toString() {
    // TODO: implement toString
    return "CartModel: id =$id, name =$name, price=$price, quantity=$quantity, isExist=$isExist, time=$time, productName =" +
        product!.name.toString() +
        ", productId=" +
        product!.id.toString() +
        ", productDescription=" +
        product!.description.toString() +
        ", productCreateAt=" +
        product!.createAt.toString() +
        ", productImg=" +
        product!.img.toString() +
        ", productLocation=" +
        product!.location.toString() +
        ", productPrice=" +
        product!.price.toString() +
        ", productStars=" +
        product!.stars.toString() +
        ", productTypeId=" +
        product!.typeId.toString() +
        ", productUpdateAt=" +
        product!.updateAt.toString();
  }
}
