class MedicalRecordsModel {
  String? id;
  String? category;
  String? fileName;
  String? dateTimestamp;
  String? description;
  String? upload;
  String? patientName;
  String? age;
  String? address;
  dynamic userId;
  int? patientId;
  bool? isActive;
  MedicalRecordsModel(
    this.id,
    this.category,
    this.fileName,
    this.dateTimestamp,
    this.description,
    this.upload,
    this.patientName,
    this.age,
    this.address,
    this.userId,
    this.patientId,
    this.isActive,
  );

  MedicalRecordsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    category = json['category'];
    fileName = json['fileName'];
    dateTimestamp = json['dateTimestamp'];
    description = json['description'];
    upload = json['upload'];
    patientName = json['patientName'];
    age = json['age'];
    address = json['address'];
    userId = json['userId'];
    patientId = json['patientId'];
    isActive = json['isActive'];
  }
}
