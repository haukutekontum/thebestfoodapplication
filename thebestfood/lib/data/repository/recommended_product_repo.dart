import 'package:get/get.dart';
import 'package:thebestfood/data/api/api_client.dart';
import 'package:thebestfood/utils/app_constants.dart';

class RecommendedProductRepo extends GetxService {
  final ApiClient apiClient;

  RecommendedProductRepo({required this.apiClient});

  Future<Response> getRecommendedProduct() async {
    return await apiClient
        .getData(AppConstants.RECOMMENDED_PRODUCT_URL);
  }
}
