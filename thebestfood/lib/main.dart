import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:thebestfood/controllers/cart_controller.dart';
import 'package:thebestfood/controllers/popular_product_controller.dart';
import 'package:thebestfood/controllers/recommended_product_controller.dart';
import 'package:thebestfood/pages/cart/cart_page.dart';
import 'package:thebestfood/pages/food/popular_food_detail.dart';
import 'package:thebestfood/pages/food/recommended_food_detail.dart';
import 'package:thebestfood/pages/home/food_page_body.dart';
import 'package:thebestfood/pages/home/main_food_page.dart';
import 'package:get/get.dart';
import 'package:thebestfood/pages/splash/splash_page.dart';
import 'package:thebestfood/routes/route_helper.dart';
import 'helper/dependencies.dart' as dep;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dep.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Get.find<CartController>().getCartData();
    return GetBuilder<PopularProductController>(
      builder: (_) {
        return GetBuilder<RecommendedProductController>(builder: (_) {
          return GetMaterialApp(
            scrollBehavior: MaterialScrollBehavior().copyWith(
              dragDevices: {
                PointerDeviceKind.mouse,
                PointerDeviceKind.touch,
                PointerDeviceKind.stylus,
                PointerDeviceKind.unknown,
              },
            ),
            title: 'Flutter Demo',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              useMaterial3: true,
            ),
            // home: CartPage(),
            initialRoute: RouteHelper.getSplashPage(),
            debugShowCheckedModeBanner: false,
            // initialRoute: RouteHelper.initial,
            getPages: RouteHelper.routes,
          );
        });
      },
    );
  }
}
