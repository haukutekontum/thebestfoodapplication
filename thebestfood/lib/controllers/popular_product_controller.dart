import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:thebestfood/controllers/cart_controller.dart';
import 'package:thebestfood/data/repository/popular_product_repo.dart';
import 'package:thebestfood/models/cart_model.dart';
import 'package:thebestfood/models/products_model.dart';
import 'package:thebestfood/utils/colors.dart';

class PopularProductController extends GetxController {
  final PopularProductRepo popularProductRepo;

  PopularProductController(
      {required this.popularProductRepo});

  List<ProductModel> _popularProductList = [];
  List<ProductModel> get popularProductList =>
      _popularProductList;
  late CartController _cart;

  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

  int _quantity = 0;
  int get quantity => _quantity;

  int _inCartItems = 0;
  int get inCartItem => _inCartItems + _quantity;

  Future<void> getPopularProductList() async {
    Response response =
        await popularProductRepo.getPopularProductList();
    if (response.statusCode == 200) {
      _popularProductList = <ProductModel>[].obs;
      List<dynamic> obj = response.body;
      // Product.fromJson(response.body).products;
      for (var o in obj) {
        List<ProductModel> products =
            Product.fromJson(o).products;
        _popularProductList.addAll(products);
        update();
      }
      if (_popularProductList.length > 0) {
        _isLoaded = true;
      }
    } else {}
  }

  void setQuantity(bool isIncrement) {
    if (isIncrement) {
      _quantity = checkQuantity(quantity + 1);
    } else {
      _quantity = checkQuantity(quantity - 1);
    }
    update();
  }

  checkQuantity(int quantity) {
    if ((_inCartItems + _quantity) < 0) {
      Get.snackbar("Item count", "You can't reduce more !",
          backgroundColor: AppColors.mainColor,
          colorText: Colors.white);
      if (_inCartItems > 0) {
        _quantity = -_inCartItems;
        return _quantity;
      }
      return 0;
    } else if ((_inCartItems + _quantity) > 20) {
      Get.snackbar("Item count", "You can't add more !",
          backgroundColor: AppColors.mainColor,
          colorText: Colors.white);
      return 20;
    } else {
      return quantity;
    }
  }

  void initProduct(ProductModel product, cart) {
    _quantity = 0;
    _inCartItems = 0;
    _cart = cart;
    var exist = false;
    exist = _cart.existInCart(product);
    //if exist
    //Get from storage _inCartItem = 3
    print("exist or not " + exist.toString());
    if (exist) {
      _inCartItems = _cart.getQuanity(product);
    }
    print("The quantity in the cart is " +
        _inCartItems.toString());
  }

  void addItem(ProductModel product) {
    _cart.addItem(product, _quantity);
    _quantity = 0;
    _inCartItems = _cart.getQuanity(product);
    update();
  }

  List<CartModel> get getItems{
    return _cart.getItems;
  }

  int get totalItems {
    return _cart.totalItems;
  }
}
