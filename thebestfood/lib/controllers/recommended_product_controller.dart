import 'package:get/get.dart';
import 'package:thebestfood/data/repository/recommended_product_repo.dart';
import 'package:thebestfood/models/products_model.dart';

class RecommendedProductController extends GetxController {
  final RecommendedProductRepo recommendedProductRepo;

  RecommendedProductController(
      {required this.recommendedProductRepo});

  List<ProductModel> _recommendedProductList = [];
  List<ProductModel> get recommendedProductList =>
      _recommendedProductList;

  bool _isLoaded = false;
  bool get isLoaded => _isLoaded;

  Future<void> getRecommendedProductList() async {
    Response response = await recommendedProductRepo
        .getRecommendedProduct();
    if (response.statusCode == 200) {
      _recommendedProductList = <ProductModel>[].obs;
      List<dynamic> obj = response.body;
      for (var o in obj) {
        List<ProductModel> products =
            Product.fromJson(o).products;
        _recommendedProductList.addAll(products);
        update();
      }
      if(_recommendedProductList.length>0){
        _isLoaded = true;
      }
    } else {}
  }
}
