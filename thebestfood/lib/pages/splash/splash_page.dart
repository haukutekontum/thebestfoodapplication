import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:thebestfood/controllers/popular_product_controller.dart';
import 'package:thebestfood/controllers/recommended_product_controller.dart';
import 'package:thebestfood/routes/route_helper.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SpashScreenState();
}

class _SpashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;

  _loadResource() async {
    await Get.find<PopularProductController>().getPopularProductList();
    await Get.find<RecommendedProductController>().getRecommendedProductList();
  }

  @override
  void initState() {
    _loadResource();
    /*
    AnyClass(){
      newObject(){
        return ..
      }
    }

    var x = AnyClass()..newObject()
    x = x.newObject()
     */

    super.initState();
    controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 2))
          ..forward();
    animation = CurvedAnimation(
      parent: controller,
      curve: Curves.linear,
    );

    Timer(const Duration(seconds: 3),
        () => Get.offNamed(RouteHelper.getInitial()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        ScaleTransition(
          scale: animation,
          child: Center(
            child: Image.asset(
              "assets/images/logo.jpg",
            ),
          ),
        )
      ]),
    );
  }
}
