import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:thebestfood/controllers/cart_controller.dart';
import 'package:thebestfood/controllers/popular_product_controller.dart';
import 'package:thebestfood/pages/cart/cart_page.dart';
import 'package:thebestfood/pages/home/main_food_page.dart';
import 'package:thebestfood/routes/route_helper.dart';
import 'package:thebestfood/utils/colors.dart';
import 'package:thebestfood/utils/dimension.dart';
import 'package:thebestfood/widgets/app_column.dart';
import 'package:thebestfood/widgets/app_icon.dart';
import 'package:thebestfood/widgets/big_text.dart';
import 'package:thebestfood/widgets/expandable_text_widget.dart';
import 'package:thebestfood/widgets/icon_and_text_widget.dart';
import 'package:thebestfood/widgets/small_text.dart';

class PopularFoodDetail extends StatelessWidget {
  int pageId;
  final String page;

  PopularFoodDetail({Key? key, required this.pageId, required this.page})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var product =
        Get.find<PopularProductController>().popularProductList[pageId];
    Get.find<PopularProductController>()
        .initProduct(product, Get.find<CartController>());

    //return front end design
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          //background image
          Positioned(
              left: 0,
              right: 0,
              child: Container(
                width: double.maxFinite,
                height: Dimension.popularFoodImgSize,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(product.img.toString()),
                        fit: BoxFit.cover)),
              )),
          //icon widgets
          Positioned(
              top: Dimension.height45,
              left: Dimension.width20,
              right: Dimension.width20,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      if (page == "cartpage") {
                        Get.toNamed(RouteHelper.getCartPage());
                      } else {
                        Get.toNamed(RouteHelper.getInitial());
                      }
                    },
                    child: AppIcon(icon: Icons.arrow_back_ios),
                  ),
                  GetBuilder<PopularProductController>(builder: (controller) {
                    return Stack(
                      children: [
                        GestureDetector(
                          onTap: () {
                            if (controller.totalItems >= 1) {
                              Get.toNamed(RouteHelper.getCartPage());
                            }
                          },
                          child: AppIcon(icon: Icons.shopping_cart_outlined),
                        ),
                        controller.totalItems >= 1
                            ? Positioned(
                                right: 0,
                                top: 0,
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    AppIcon(
                                      icon: Icons.circle,
                                      size: 20,
                                      iconColor: Colors.transparent,
                                      backgroundColor: AppColors.mainColor,
                                    ),
                                    BigText(
                                      text: controller.totalItems.toString(),
                                      size: 12,
                                      color: Colors.white,
                                    )
                                  ],
                                ))
                            : Container(),
                      ],
                    );
                  })
                ],
              )),
          //introduction of food
          Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              top: Dimension.popularFoodImgSize - 20,
              child: Container(
                  padding: EdgeInsets.only(
                      left: Dimension.width20,
                      right: Dimension.width20,
                      top: Dimension.height20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(Dimension.radious20),
                          topRight: Radius.circular(Dimension.radious20)),
                      color: Colors.white),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppColumn(
                        text: product.name.toString(),
                      ),
                      SizedBox(
                        height: Dimension.height20,
                      ),
                      BigText(text: "Introduce"),
                      SizedBox(
                        height: Dimension.height20,
                      ),
                      //expandable text widget
                      Expanded(
                          child: SingleChildScrollView(
                        child: ExpandableTextWidget(
                            text: product.description.toString()),
                      ))
                    ],
                  ))),
        ],
      ),
      bottomNavigationBar:
          GetBuilder<PopularProductController>(builder: (popularProduct) {
        return Container(
          width: double.infinity,
          height: Dimension.bottomHeightBar,
          padding: EdgeInsets.only(
              top: Dimension.height30,
              bottom: Dimension.height30,
              left: Dimension.width20,
              right: Dimension.width20),
          decoration: BoxDecoration(
              color: AppColors.buttonBackgroundColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(Dimension.radious20 * 2),
                  topRight: Radius.circular(Dimension.radious20 * 2))),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Container(
              padding: EdgeInsets.all(Dimension.height20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Dimension.radious20),
                  color: Colors.white),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      popularProduct.setQuantity(false);
                    },
                    child: Icon(
                      Icons.remove,
                      color: AppColors.signColor,
                    ),
                  ),
                  SizedBox(
                    width: Dimension.width10 / 2,
                  ),
                  BigText(text: popularProduct.inCartItem.toString()),
                  SizedBox(
                    width: Dimension.width10 / 2,
                  ),
                  GestureDetector(
                    onTap: () {
                      popularProduct.setQuantity(true);
                    },
                    child: Icon(
                      Icons.add,
                      color: AppColors.signColor,
                    ),
                  )
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                popularProduct.addItem(product);
              },
              child: Container(
                padding: EdgeInsets.all(Dimension.height20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Dimension.radious20),
                  color: AppColors.mainColor,
                ),
                child: BigText(
                  text: "\$${product.price} | Add to cart",
                  color: Colors.white,
                ),
              ),
            ),
          ]),
        );
      }),
    );
  }
}
