import 'package:flutter/material.dart';
import 'package:thebestfood/pages/home/food_page_body.dart';
import 'package:thebestfood/utils/colors.dart';
import 'package:thebestfood/utils/dimension.dart';
import 'package:thebestfood/widgets/big_text.dart';
import 'package:thebestfood/widgets/small_text.dart';

class MainFoodPage extends StatefulWidget {
  const MainFoodPage({Key? key}) : super(key: key);

  @override
  _MainFoodPageState createState() => _MainFoodPageState();
}

class _MainFoodPageState extends State<MainFoodPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        //Showing the header
        Container(
          child: Container(
            margin: EdgeInsets.only(
                top: Dimension.height45,
                bottom: Dimension.height15),
            padding: EdgeInsets.only(
                left: Dimension.width20,
                right: Dimension.width20),
            child: Row(
                mainAxisAlignment:
                    MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      BigText(
                        text: "Bangladesh",
                        color: AppColors.mainColor,
                      ),
                      Row(
                        children: [
                          SmallText(
                            text: "Narsingdi",
                            color: Colors.black54,
                          ),
                          Icon(
                              Icons.arrow_drop_down_rounded)
                        ],
                      )
                    ],
                  ),
                  Center(
                    child: Container(
                      width: Dimension.width45,
                      height: Dimension.height45,
                      child: Icon(Icons.search,
                          color: Colors.white,
                          size: Dimension.iconSize24),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(
                            Dimension.radious15),
                        color: AppColors.mainColor,
                      ),
                    ),
                  )
                ]),
          ),
        ),

        //Showing the body
        const Expanded(
            child: SingleChildScrollView(
          child: FoodPageBody(),
        ))
      ],
    ));
  }
}
