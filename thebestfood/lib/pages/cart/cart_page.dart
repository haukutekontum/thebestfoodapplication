import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:thebestfood/base/no_data_page.dart';
import 'package:thebestfood/controllers/cart_controller.dart';
import 'package:thebestfood/controllers/popular_product_controller.dart';
import 'package:thebestfood/controllers/recommended_product_controller.dart';
import 'package:thebestfood/models/cart_model.dart';
import 'package:thebestfood/pages/home/main_food_page.dart';
import 'package:thebestfood/routes/route_helper.dart';
import 'package:thebestfood/utils/colors.dart';
import 'package:thebestfood/utils/dimension.dart';
import 'package:thebestfood/widgets/app_icon.dart';
import 'package:thebestfood/widgets/big_text.dart';
import 'package:thebestfood/widgets/small_text.dart';

class CartPage extends StatelessWidget {
  const CartPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          //header
          Positioned(
              top: Dimension.height20 * 2,
              left: Dimension.width20,
              right: Dimension.width20,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: AppIcon(
                      icon: Icons.arrow_back_ios,
                      iconColor: Colors.white,
                      backgroundColor: AppColors.mainColor,
                      iconSize: Dimension.iconSize24,
                    ),
                  ),
                  SizedBox(width: Dimension.width20 * 5),
                  GestureDetector(
                    onTap: () {
                      Get.toNamed(RouteHelper.getInitial());
                    },
                    child: AppIcon(
                      icon: Icons.home_outlined,
                      iconColor: Colors.white,
                      backgroundColor: AppColors.mainColor,
                      iconSize: Dimension.iconSize24,
                    ),
                  ),
                  AppIcon(
                    icon: Icons.shopping_cart,
                    iconColor: Colors.white,
                    backgroundColor: AppColors.mainColor,
                    iconSize: Dimension.iconSize24,
                  )
                ],
              )),

          //body
          GetBuilder<CartController>(builder: (_cartController) {
            return _cartController.getItems.length > 0
                ? Positioned(
                    top: Dimension.height20 * 5,
                    left: Dimension.width20,
                    right: Dimension.width20,
                    bottom: 0,
                    child: Container(
                        // color: Colors.red,
                        child: Container(
                            margin: EdgeInsets.only(top: Dimension.height15),
                            child: GetBuilder<CartController>(
                                builder: (cartController) {
                              var _cartList = cartController.getItems;
                              return ListView.builder(
                                  itemCount: _cartList.length,
                                  itemBuilder: (_, index) {
                                    return Container(
                                        height: 100,
                                        width: double.maxFinite,
                                        child: GestureDetector(
                                          onTap: () {
                                            var popularIndex = Get.find<
                                                    PopularProductController>()
                                                .popularProductList
                                                .indexOf(
                                                    _cartList[index].product!);
                                            if (popularIndex >= 0) {
                                              Get.toNamed(
                                                  RouteHelper.getPopularFood(
                                                      popularIndex,
                                                      "cartpage"));
                                            } else {
                                              var recommendedIndex = Get.find<
                                                      RecommendedProductController>()
                                                  .recommendedProductList
                                                  .indexOf(_cartList[index]
                                                      .product!);
                                              if (recommendedIndex < 0) {
                                                Get.snackbar("History product",
                                                    "Product review is not avaible for history products");
                                                // print("recommendedIndex " +
                                                //     recommendedIndex.toString());
                                                // print("Clicked for cart list " +
                                                //     _cartList[index].toString());
                                                // print("Clicked for recommended list + " +
                                                //     Get.find<
                                                //             RecommendedProductController>()
                                                //         .recommendedProductList
                                                //         .length
                                                //         .toString());
                                              } else {
                                                Get.toNamed(RouteHelper
                                                    .getRecommendedFood(
                                                        recommendedIndex,
                                                        "cartpage"));
                                                print("Clicked " +
                                                    recommendedIndex
                                                        .toString());
                                              }
                                            }
                                          },
                                          child: Row(
                                            children: [
                                              Container(
                                                width: Dimension.height20 * 5,
                                                height: Dimension.height20 * 5,
                                                margin: EdgeInsets.only(
                                                    bottom: Dimension.height10),
                                                decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                        image: AssetImage(
                                                            cartController
                                                                .getItems[index]
                                                                .product!
                                                                .img
                                                                .toString()
                                                                .trim()),
                                                        fit: BoxFit.cover),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            Dimension
                                                                .radious20),
                                                    color: Colors.white),
                                              ),
                                              SizedBox(
                                                width: Dimension.width10,
                                              ),
                                              Expanded(
                                                  child: Container(
                                                height: Dimension.height20 * 5,
                                                child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: [
                                                      BigText(
                                                        text: cartController
                                                            .getItems[index]
                                                            .name!,
                                                        color: Colors.black54,
                                                      ),
                                                      SmallText(text: "Spicy"),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          BigText(
                                                            text:
                                                                "\$ ${cartController.getItems[index].price.toString()}",
                                                            color: Colors
                                                                .redAccent,
                                                          ),
                                                          Container(
                                                            padding: EdgeInsets
                                                                .all(Dimension
                                                                    .height10),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius.circular(
                                                                        Dimension
                                                                            .radious20),
                                                                color: Colors
                                                                    .white),
                                                            child: Row(
                                                              children: [
                                                                GestureDetector(
                                                                  onTap: () {
                                                                    cartController.addItem(
                                                                        _cartList[index]
                                                                            .product!,
                                                                        -1);
                                                                  },
                                                                  child: Icon(
                                                                    Icons
                                                                        .remove,
                                                                    color: AppColors
                                                                        .signColor,
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                  width: Dimension
                                                                          .width10 /
                                                                      2,
                                                                ),
                                                                BigText(
                                                                    text: _cartList[
                                                                            index]
                                                                        .quantity
                                                                        .toString()),
                                                                SizedBox(
                                                                  width: Dimension
                                                                          .width10 /
                                                                      2,
                                                                ),
                                                                GestureDetector(
                                                                  onTap: () {
                                                                    cartController.addItem(
                                                                        _cartList[index]
                                                                            .product!,
                                                                        1);
                                                                  },
                                                                  child: Icon(
                                                                    Icons.add,
                                                                    color: AppColors
                                                                        .signColor,
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      )
                                                    ]),
                                              ))
                                            ],
                                          ),
                                        ));
                                  });
                            }))))
                : NoDataPage(text: "Your cart is empty!");
          })
        ],
      ),
      bottomNavigationBar:
          GetBuilder<CartController>(builder: (cartController) {
        return Container(
            width: double.infinity,
            height: Dimension.bottomHeightBar,
            padding: EdgeInsets.only(
                top: Dimension.height30,
                bottom: Dimension.height30,
                left: Dimension.width20,
                right: Dimension.width20),
            decoration: BoxDecoration(
                color: AppColors.buttonBackgroundColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(Dimension.radious20 * 2),
                    topRight: Radius.circular(Dimension.radious20 * 2))),
            child: cartController.getItems.length > 0
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                        Container(
                          padding: EdgeInsets.all(Dimension.height20),
                          margin: EdgeInsets.only(left: Dimension.width15),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(Dimension.radious20),
                              color: Colors.white),
                          child: BigText(
                              text: "\$ " +
                                  cartController.totalAmount.toString()),
                        ),
                        GestureDetector(
                          onTap: () {
                            cartController.addToHistory();
                          },
                          child: Container(
                            padding: EdgeInsets.all(Dimension.height20),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(Dimension.radious20),
                              color: AppColors.mainColor,
                            ),
                            child: BigText(
                              text: "Check out",
                              color: Colors.white,
                            ),
                          ),
                        )
                      ])
                : Container());
      }),
    );
  }
}
