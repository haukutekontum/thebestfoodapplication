import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:thebestfood/base/no_data_page.dart';
import 'package:thebestfood/controllers/cart_controller.dart';
import 'package:thebestfood/models/cart_model.dart';
import 'package:thebestfood/routes/route_helper.dart';
import 'package:thebestfood/utils/app_constants.dart';
import 'package:thebestfood/utils/colors.dart';
import 'package:thebestfood/utils/dimension.dart';
import 'package:thebestfood/widgets/app_icon.dart';
import 'package:thebestfood/widgets/big_text.dart';
import 'package:thebestfood/widgets/small_text.dart';

class CartHistory extends StatelessWidget {
  const CartHistory({super.key});

  @override
  Widget build(BuildContext context) {
    var getCartHistoryList =
        Get.find<CartController>().getCartHistoryList().reversed.toList();

    Map<String, int> cartItemsPerOrder = Map();

    for (int i = 0; i < getCartHistoryList.length; i++) {
      if (cartItemsPerOrder.containsKey(getCartHistoryList[i].time)) {
        cartItemsPerOrder.update(
            getCartHistoryList[i].time!, (value) => ++value);
      } else {
        cartItemsPerOrder.putIfAbsent(getCartHistoryList[i].time!, () => 1);
      }
    }

    List<int> cartItemPerOrderToList() {
      return cartItemsPerOrder.entries.map((e) => e.value).toList();
    }

    List<int> itemsPerOrder = cartItemPerOrderToList(); //3, 2, 3

    var listCounter = 0;

    List<String> cartOrderTimeToList() {
      return cartItemsPerOrder.entries.map((e) => e.key).toList();
    }

    Widget timeWidget(int index) {
      var outputDate = DateTime.now().toString();
      if (index < getCartHistoryList.length) {
        DateTime parseData = DateFormat("yyy-MM-dd HH:mm:ss")
            .parse(getCartHistoryList[listCounter].time!.toString());
        var outputFormat = DateFormat("dd/MM/yyyy hh:mm a");
        outputDate = outputFormat.format(parseData);
      }
      return Text(outputDate);
    }

    return Scaffold(
      body: Column(
        children: [
          //header
          Container(
            color: AppColors.mainColor,
            width: double.maxFinite,
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  BigText(
                    text: "Cart History",
                    color: Colors.white,
                  ),
                  AppIcon(
                    icon: Icons.shopping_cart_outlined,
                    iconColor: AppColors.mainColor,
                  ),
                ]),
          ),

          //body
          GetBuilder<CartController>(builder: (_cartController) {
            return _cartController.getCartHistoryList().length > 0
                ? Expanded(
                    child: Container(
                        height: 500,
                        margin: EdgeInsets.only(
                            top: Dimension.height20,
                            left: Dimension.width20,
                            right: Dimension.width20),
                        child: MediaQuery.removePadding(
                          removeTop: true,
                          context: context,
                          child: ListView(
                            children: [
                              for (int i = 0; i < itemsPerOrder.length; i++)
                                Container(
                                  height: 120,
                                  margin: EdgeInsets.only(
                                      bottom: Dimension.height20),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      timeWidget(listCounter),
                                      // (() {
                                      //   DateTime parseData = DateFormat(
                                      //           "yyy-MM-dd HH:mm:ss")
                                      //       .parse(
                                      //           getCartHistoryList[listCounter]
                                      //               .time!
                                      //               .toString());
                                      //   var outputFormat =
                                      //       DateFormat("dd/MM/yyyy hh:mm a");
                                      //   var outputDate =
                                      //       outputFormat.format(parseData);
                                      //   return Text(outputDate);
                                      // }()),
                                      SizedBox(
                                        height: Dimension.height10,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Wrap(
                                              direction: Axis.horizontal,
                                              children: List.generate(
                                                  itemsPerOrder[i], (index) {
                                                if (listCounter <
                                                    getCartHistoryList.length) {
                                                  listCounter++;
                                                }
                                                return index <= 2
                                                    ? Container(
                                                        height: 80,
                                                        width: 80,
                                                        margin: EdgeInsets.only(
                                                            right: Dimension
                                                                    .width10 /
                                                                2),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius: BorderRadius
                                                              .circular(Dimension
                                                                      .radious15 /
                                                                  2),
                                                          image: DecorationImage(
                                                              fit: BoxFit.cover,
                                                              image: AssetImage(
                                                                  getCartHistoryList[
                                                                          listCounter -
                                                                              1]
                                                                      .product!
                                                                      .img
                                                                      .toString())),
                                                        ),
                                                      )
                                                    : Container();
                                              })),
                                          Container(
                                            height: 80,
                                            child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  SmallText(
                                                      text: "Total",
                                                      color:
                                                          AppColors.titleColor),
                                                  BigText(
                                                    text: itemsPerOrder[i]
                                                            .toString() +
                                                        " Items",
                                                    color: AppColors.titleColor,
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      var ordertime =
                                                          cartOrderTimeToList();
                                                      Map<int, CartModel>
                                                          moreOrder = {};
                                                      for (int j = 0;
                                                          j <
                                                              getCartHistoryList
                                                                  .length;
                                                          j++) {
                                                        if (getCartHistoryList[
                                                                    j]
                                                                .time ==
                                                            ordertime[i]) {
                                                          moreOrder.putIfAbsent(
                                                              getCartHistoryList[
                                                                      j]
                                                                  .id!,
                                                              () => CartModel.fromJson(
                                                                  jsonDecode(jsonEncode(
                                                                      getCartHistoryList[
                                                                          j]))));
                                                          Get.find<CartController>()
                                                                  .setItems =
                                                              moreOrder;
                                                          Get.find<
                                                                  CartController>()
                                                              .addToCartList();
                                                              Get.toNamed(RouteHelper
                                                        .getCartPage());

                                                        }
                                                      }
                                                    },
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal:
                                                                  Dimension
                                                                      .width10,
                                                              vertical: Dimension
                                                                      .height10 /
                                                                  2),
                                                      decoration: BoxDecoration(
                                                          borderRadius: BorderRadius
                                                              .circular(Dimension
                                                                      .radious15 /
                                                                  2),
                                                          border: Border.all(
                                                              width: 1,
                                                              color: AppColors
                                                                  .mainColor)),
                                                      child: SmallText(
                                                        text: "One more",
                                                        color:
                                                            AppColors.mainColor,
                                                      ),
                                                    ),
                                                  )
                                                ]),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                )
                            ],
                          ),
                        )))
                : Container(
                    height: MediaQuery.of(context).size.height / 1.5,
                    child: Center(
                      child:
                          NoDataPage(text: "Your didn't buy anything so far !"),
                    ),
                  );
          })
        ],
      ),
    );
  }
}
